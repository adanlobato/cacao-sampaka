$(document).ready(function(){
    $('.nav-slider a, .caja-presentacion .bt-atreves a').click(function(e){
        e.preventDefault();

        var id = $(this).attr('href');
        var slideNum = id.split('-')[1];

        $('.nav-slider a').removeClass('active');
        $('.nav-slider a[href="' + id + '"]').addClass('active');

        $('.slider-inner').animate({ marginLeft: -932 * slideNum }, {
            duration: 'slow',
            easing: 'easeOutCubic'
        });

        $('.slider-background:visible').fadeOut('slow');
        $('#slider-background-' + slideNum).fadeIn('slow');
    });

    $('.contenidos.slider .draggable').draggable({ revert: true, axis: 'x', containment: ".contenidos.slider" });
    $('.chocolates-with-cover .draggable').draggable({
        /*helper: 'clone',
        start: function(){
            $(this).hide();
        },
        stop: function(){
            $(this).show()
        },*/
        revert: true,
        //appendTo: 'body',
        containment: ".slider"
    });

    $('.expand-news').click(function(e){
        e.preventDefault();
        $(this).parents('li').find('.full-description').toggle();

        if ($(this).parents('li').find('.full-description:hidden').length) {
            $(this).parents('li').find('.expand-news.mini').first().show();
            $(this).parents('li').find('.full-description .expand-news.mini').hide();
        } else {
            $(this).parents('li').find('.expand-news.mini').first().hide();
            $(this).parents('li').find('.full-description .expand-news.mini').show();
        }
    });

    $('.news-content-history h2 a').live('click', function(e){
        e.preventDefault();
        $(this).parents('li').find('.blackbox').fadeIn();
        $('.blackbox-overlay').fadeIn();
        $.scrollTo($(this), 1000, {axis:'y'});
    });

    $('.news-content-history a.close').live('click', function(e){
        e.preventDefault();
        $(this).parents('li').find('.blackbox').fadeOut();
        $('.blackbox-overlay').fadeOut();
    });

    // Generar demo
    $('.news-content-history li').each(function(){
        if (!$(this).find('.blackbox').length) {
            $(this).append('<div class="blackbox">' + $('.news-content-history li:first .blackbox').html() + '</div>');
        }
    });

    // Generic slideshow
    $('.generic-slideshow .generic-slideshow-prev').click(function(e){
        e.preventDefault();
        var innerDiv = $(this).parents('.generic-slideshow').find('.generic-slideshow-inner');
        var viewportDiv = $(this).parents('.generic-slideshow').find('.generic-slideshow-viewport');

        var currentMargin = parseInt(innerDiv.css('margin-left').replace('px',''));
        var totalWidth = parseInt('-' + viewportDiv.width() * (innerDiv.find('.generic-slide').length - 1));
        if (currentMargin < 0) {
            innerDiv.animate({ marginLeft: '+=' + viewportDiv.width() }, {
                duration: 'slow',
                easing: 'easeOutCubic',
                complete: function(){
                    var page = Math.round(Math.abs(parseInt(innerDiv.css('margin-left').replace('px', '')) / viewportDiv.width()));
                    $('.generic-slideshow-pagination ul li').removeClass('active');
                    $('.generic-slideshow-pagination ul li:eq(' + page + ')').addClass('active');
                }
            });
        } else {
            innerDiv.animate({ marginLeft: '+=' + totalWidth }, {
                duration: 'slow',
                easing: 'easeOutCubic',
                complete: function(){
                    var page = Math.round(Math.abs(parseInt(innerDiv.css('margin-left').replace('px', '')) / viewportDiv.width()));
                    $('.generic-slideshow-pagination ul li').removeClass('active');
                    $('.generic-slideshow-pagination ul li:eq(' + page + ')').addClass('active');
                }
            });
        }
    });

    $('.generic-slideshow .generic-slideshow-next').click(function(e){
        e.preventDefault();
        var innerDiv = $(this).parents('.generic-slideshow').find('.generic-slideshow-inner');
        var viewportDiv = $(this).parents('.generic-slideshow').find('.generic-slideshow-viewport');

        var currentMargin = parseInt(innerDiv.css('margin-left').replace('px',''));
        var totalWidth = parseInt('-' + viewportDiv.width() * (innerDiv.find('.generic-slide').length - 1));
        if (currentMargin > totalWidth) {
            innerDiv.animate({ marginLeft: '-=' + viewportDiv.width() }, {
                duration: 'slow',
                easing: 'easeOutCubic',
                complete: function(){
                    var page = Math.round(Math.abs(parseInt(innerDiv.css('margin-left').replace('px', '')) / viewportDiv.width()));
                    $('.generic-slideshow-pagination ul li').removeClass('active');
                    $('.generic-slideshow-pagination ul li:eq(' + page + ')').addClass('active');
                }
            });
        } else {
            innerDiv.animate({ marginLeft: 0 }, {
                duration: 'slow',
                easing: 'easeOutCubic',
                complete: function(){
                    var page = Math.round(Math.abs(parseInt(innerDiv.css('margin-left').replace('px', '')) / viewportDiv.width()));
                    $('.generic-slideshow-pagination ul li').removeClass('active');
                    $('.generic-slideshow-pagination ul li:eq(' + page + ')').addClass('active');
                }
            });
        }
    });

    $('.generic-slideshow .generic-slideshow-pagination li').click(function(e){
        e.preventDefault();

        var page = $(this).index();
        $('.generic-slideshow-pagination ul li').removeClass('active');
        $('.generic-slideshow-pagination ul li:eq(' + page + ')').addClass('active');

        var innerDiv = $(this).parents('.generic-slideshow').find('.generic-slideshow-inner');
        var viewportDiv = $(this).parents('.generic-slideshow').find('.generic-slideshow-viewport');

        innerDiv.animate({ marginLeft: (viewportDiv.width() * (page * -1)) }, {
            duration: 'slow',
            easing: 'easeOutCubic'
        });
    });

    $.datepicker.setDefaults( $.datepicker.regional[ "es" ] );
    $('div.cuadro-registro-2 div.calendario').datepicker($.datepicker.regional[ "es" ]);

    /* Chocometro */
    $( "#chocometro" ).slider({
        value: 0,
        stop: function( event, ui ) {
            var slide = Math.round(ui.value / 20);
            $('#chocometro-wrapper').removeClass().addClass('slide' + slide);
            $('#chocometro-wrapper h2').removeClass('active');
            $('#chocometro-wrapper h2.title-slide' + slide).addClass('active');
            $('.slider-inner').animate({ marginLeft: -932 * slide }, {
                duration: 'slow',
                easing: 'easeOutCubic'
            });
            $('#chocometro a').animate({ left: slide * 20 + '%' }, {
                duration: 'slow',
                easing: 'easeOutCubic'
            });
        }
    });


    $('.go-to-slide a').click(function(e){
        e.preventDefault();

        var id = $(this).attr('href');
        var slideNum = id.split('-')[1];

        $('#chocometro a').animate({ left: slideNum * 20 + '%' }, {
            duration: 'slow',
            easing: 'easeOutCubic',
            complete: function () {
                $( "#chocometro" ).slider('value', slideNum * 20);
            }
        });

        $('#chocometro-wrapper').removeClass().addClass('slide' + slideNum);
        $('#chocometro-wrapper h2').removeClass('active');
        $('#chocometro-wrapper h2.title-slide' + slideNum).addClass('active');

        $('.slider-inner').animate({ marginLeft: -932 * slideNum }, {
            duration: 'slow',
            easing: 'easeOutCubic'
        });
    });


    /* Improvisación */
    $('.mini-go-to-slide a').click(function(e){
        e.preventDefault();
        $('.customizable-box .controls').fadeIn('fast');

        var move;
        if ($(this).parent().hasClass('prev')) move = '+=640';
        else move = '-=640';

        $('.mini-slider-inner').animate({ marginLeft: move }, {
            duration: 'slow',
            easing: 'easeOutCubic'
        });
    });

    function calculateBox() {
        var boxPrice = 8.00;
        var currentPrice = 0;
        $('.collection-dropped-chocolate').each(function() {
            var choco = $(this);
            currentPrice = currentPrice + parseFloat($(choco).attr('price'));
        });

        return Math.round( (currentPrice + boxPrice) * 100 ) / 100;
    }

    $('.customizable-box-inner').droppable({
        accept: ".collection-draggable-chocolate",
        activeClass: "ui-state-hover",
        hoverClass: "ui-state-active",
        drop: function( event, ui ) {
            if ($(this).find('.collection-dropped-chocolate').length == 16) return;

            $(this)
                .addClass( "ui-state-highlight" )
                .append('<div class="collection-dropped-chocolate" price="' + $(ui.draggable).attr('price') + '">' + ui.draggable.html() + '</div>');

            if ($(this).find('.collection-dropped-chocolate').length == 16) {
                $(this).parent().find('.cover').fadeIn();
                $(this).parent().find('.btn-choco-comprar').removeClass('disabled');
            } else {
                $(this).parent().find('.btn-choco-comprar').addClass('disabled');
                // Cambiar precio aquí, por ejemplo:
                var currentPrice = calculateBox();
                $(this).parent().find('span span').text(currentPrice);
            }
        }
    });

    $('.collection-draggable-chocolate').draggable({
        helper: 'clone',
        revert: 'invalid',
        appendTo: 'body',
        start: function(e, ui){
            $(ui.helper).addClass('ui-moving');
            $('.chocolate-tooltip').hide();
        },
        stop: function(e, ui){
            $(ui.helper).removeClass('ui-moving');
        }
    })

    $('.collection-draggable-chocolate:not(.ui-moving), .collection-dropped-chocolate').live('mouseenter', function () {
        if ($(this).hasClass('collection-dropped-chocolate')) $('.chocolate-tooltip').addClass('delete');
        else $('.chocolate-tooltip').removeClass('delete');

        $('.chocolate-tooltip')
            .hide()
            .data('item', this)
            .css('top', $(this).offset().top)
            .css('left', $(this).offset().left)
            .show();
    }).live('mouseleave', function () {
        $('.chocolate-tooltip').hide();
    });

    $('.chocolate-tooltip').live('mouseenter', function () {
        $(this).show();
    }).live('mouseleave', function () {
        $(this).hide();
    });

    $('.chocolate-tooltip a.delete').click(function(e){
        e.preventDefault();
        $($(this).parents('.chocolate-tooltip').data('item')).remove();
        $('.chocolate-tooltip').hide();

        var currentPrice = calculateBox();
        $('.customizable-box').find('span span').text(currentPrice);
    });

    /* Tienda */
    var carouselWidth = ($('.contenidos-productos li:first').width() + 4) * $('.contenidos-productos li').length;
    $('.contenidos-productos ul')
        .append($('.contenidos-productos ul').html())
        .append($('.contenidos-productos ul').html())
        .append($('.contenidos-productos ul').html())
        .css('margin-left', '-' + carouselWidth + 'px');
    $('.contenidos-productos div.caja-productos').width(carouselWidth * 10);
    $('.contenidos-productos').bind('mouseover', function(e) {
        var productList = $(this);
        var productListUl = productList.find('ul');
        clearInterval(this.iid);
        this.iid = setInterval(function() {

            var side = 'right';
            var offset = e.pageX;
            var totalWidth = Math.min(productList.width(), $(window).width());

            if (offset < totalWidth / 2) {
                side = 'left';
                offset = totalWidth - offset;
            }

            if (offset < .75 * totalWidth) {
                return;
            } else {
                var velocity = Math.floor((offset - totalWidth / 2) / (totalWidth - totalWidth / 2) * 5);

                // Emulate infinite carousel
                var marginLeft = Math.abs(parseInt(productListUl.css('margin-left')));
                if (carouselWidth * 2 <= marginLeft || marginLeft <= 0) {
                    productListUl.css('margin-left', '-' + carouselWidth + 'px');
                }

                if (side === 'left') {
                    productListUl.css('margin-left', '+=' + velocity);
                } else {
                    productListUl.css('margin-left', '-=' + velocity);
                }
            }
        }, 1);
    }).bind('mouseleave', function(e){
        this.iid && clearInterval(this.iid);
    });

    $('.contenidos-productos li').draggable({
        revert: 'invalid',
        helper: 'clone',
        opacity: 0.7,
        cursor: 'move',
        appendTo: '.contenidos-productos'
    });

    $( "div.cart" ).droppable({
        accept: ".contenidos-productos li, .customizable-box .cover",
        tolerance: 'pointer',
        drop: function( event, ui ) {
            $('#cart-popup').fadeIn();
        }
    });

    $('.customizable-box .cover').draggable({
        revert: 'invalid',
        helper: 'clone',
        opacity: 0.7,
        cursor: 'move',
        appendTo: 'body'
    });

    $('#cart-popup h3 a').click(function(e){
        e.preventDefault();
        /*$.ajax({
            type: "POST",
            url: $(this).parents('form').attr('action'),
            data: $(this).parents('form').serialize()
        }).done(function( msg ) {
            $('#cart-popup').fadeOut();
        });*/
        $('#cart-popup').fadeOut(); // Quitar cuando se active el ajax
    });

    $('.carusel-product .precio-compra a').click(function(e){
        e.preventDefault();
        var element = $(this).parents('.carusel-product');
        console.log(element.offset());
        console.log(element.position());

        element.clone().appendTo('.contenidos-productos').css({
            position: 'absolute',
            top: element.position().top,
            left: element.position().left,
            'z-index': '200'
        }).animate({
            top: '-135px',
            left: '1105px'
        }, 'slow', function(){
            $(this).remove();
            $('#cart-popup').fadeIn();
        });
    });

    $('.carusel-product a').click(function(e){
        e.preventDefault();
        if ($(this).parents('.precio-compra').length > 0) return;
        $('#ficha-tecnica-overlay').css({height: $('html').height()}).fadeIn();
        $('#ficha-tecnica').fadeIn();
    });

    $('.carusel-product .cover').hover(function(e){
        e.preventDefault();
        $(this).find('a').fadeOut();
    }, function(e){
        e.preventDefault();
        $(this).find('a').fadeIn();
    });

    $('#ficha-tecnica a.close').click(function(e){
        e.preventDefault();
        $('#ficha-tecnica-overlay').fadeOut();
        $('#ficha-tecnica').fadeOut();
    });

    $('.avanzada a').click(function(e){
        e.preventDefault();
        $(this).toggleClass('expanded');
        $('#filter-content').slideToggle();
    });

    /* Mapa tiendas */
    $('.mapa-tiendas .tienda').click(function(e){
        e.preventDefault();
        $('.tienda-info').hide();
        $(this).find('.tienda-info').fadeIn();
    });

    $('.mapa-tiendas .city-labels li a').click(function(e){
        e.preventDefault();
        var target = $(this).attr('href');
        $('.tienda-info').hide();
        $('.mapa-tiendas ' + target + ' .tienda-info').fadeIn();
    });

    $('.mapa-tiendas .city-labels li a').hover(function(e){
        e.preventDefault();
        var target = $(this).attr('href');
        $('.mapa-tiendas ' + target).addClass('hover');
    }, function(e){
        e.preventDefault();
        var target = $(this).attr('href');
        $('.mapa-tiendas ' + target).removeClass('hover');
    });

    $('.mapa-tiendas .tienda a.close').click(function(e){
        e.preventDefault();
        $('.tienda-info').fadeOut();
        e.stopPropagation();
    });

    /* Formularios */
    $('.collapsable').live('change', function(){
        if ($(this).val() > 0) {
            $('#collapsable-' + $(this).attr('name')).slideDown();
        } else {
            $('#collapsable-' + $(this).attr('name')).slideUp();
        }
    });

    $('.cuadro-cuenta tr').click(function(){
        var next = $(this).next();
        if (next.hasClass('pedido-desplegado')) {
            next.toggle();
        }
    });

    $('.cocina-content .column ul.calendar li').click(function(){
        $(this).toggleClass('expanded').find('.calendar-content').slideToggle();
    });

    /* Lupa */
    $('.bt-buscador a').click(function(e){
        e.preventDefault();
        if ($(this).parents('.bt-buscador-field').length > 0) {
            $(this).parents('.bt-buscador-field').animate({width: '11px'}, function(){
                $(this).find('form').remove();
                $(this).find('a').unwrap();
            });
        } else {
            $(this)
                .wrap('<div class="bt-buscador-field" />')
                .parents('.bt-buscador-field')
                .prepend('<form method="post" action="' + $(this).attr('href') + '"><input type="text" name="s" /></form>')
                .animate({width: '114px'});
        }
    })

});